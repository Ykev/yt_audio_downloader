import os
from pytube import Playlist, YouTube
import constants
import requests

def downloadMusicFile(url):
    stream = YouTube(url).streams.get_audio_only()
    # changing file type
    out_file = stream.download(constants.AUDIO_FOLDER)
    base, ext = os.path.splitext(out_file)
    new_file = base + '.mp3'
    os.rename(out_file, new_file)


def check_video_url(url):
    request = requests.get(url)
    print(request.status_code)
    return request.status_code == 200

def downloadFiles(link):
    if not check_video_url(link):
        print("cds")
        raise Exception("Invalid link!")
    link_list = [link]
    if 'playlist' in link:
        link_list = Playlist(link)

    for url in link_list:
        downloadMusicFile(url)
        print(url)