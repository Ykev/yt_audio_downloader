import os
import YT_Download
import constants
from telegram.ext import *
import responses

print("Hello. Cleint has just started.")


def start_command(update, context):
    update.message.reply_text(constants.HELP_RESPONSE)


def help_command(update, context):
    update.message.reply_text(constants.HELP_RESPONSE)


def handle_message(update, context):
    text = str(update.message.text)
    update.message.reply_text(constants.DOWNLOADING_RESPONSE)
    # trying to download the files
    try:
        YT_Download.downloadFiles(text)

        for file in os.scandir(constants.AUDIO_FOLDER):
            if file.is_file() and os.stat(file.path).st_size:
                print(file.path)
                chat_id = update.message.chat_id
                audio_file = open(file.path, 'rb')
                # trying to send the file
                try:
                    context.bot.send_audio(chat_id=chat_id, audio=audio_file, filename=file.name, timeout=60)
                except Exception as e:
                    update.message.reply_text(file.name + "\n" + str(e))
                audio_file.close()
                os.remove(file.path)

        update.message.reply_text(constants.FINISH_RESPONSE)
    except Exception as e:
        print(e)
        response = responses.responses(text)
        update.message.reply_text(response)


def error(update, context):
    print(f"Update {update} cause error: {context.error}")


def main():
    updater = Updater(constants.API_KEY, use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("start", start_command))
    dp.add_handler(CommandHandler("help", help_command))

    dp.add_handler(MessageHandler(Filters.text, handle_message))
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
