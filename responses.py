def responses(input_text):
    user_message = str(input_text).lower()

    if user_message in ("test", "testing"):
        return "123 working..."

    return "The command doesn't exist. Type /help to see the command options."